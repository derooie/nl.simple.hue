exports.hueLedring = function(){
    Homey.manager('ledring').animate(
    // animation name (choose from loading, pulse, progress, solid)
    'loading',

    // optional animation-specific options
    {
        color: 'blue',
        rpm: 4 // change rotations per minute
    },

    // priority
    'INFORMATIVE',

    // duration
    3000,

    // callback
    function( err, success ) {
        if( err ) return Homey.error(err);
        console.log("Animation played succesfully");
    }
);
}