"use strict";

var hueLedring = require('./ledring')

exports.hueSpeech = function () {

    Homey.manager('speech-input').on('speech', function (speech, callback) {

        var lights = false
        var brightness = null

        // loop all triggers
        speech.triggers.forEach(function (trigger) {
            if (trigger.id == 'light') {
                lights = true
            }

            if (lights && trigger.id == 'brightness') {
                // console.log('Licht zetten op ' + trigger.text + ' procent')
                brightness = Math.round(trigger.text * 2.55)

                app.get('/lights_bri/' + brightness, function (err, result) {
                    if (err) return console.error(err);
                });
            }

        });

        callback(null, true);

    });

}
