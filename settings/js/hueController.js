var app = angular.module('simpleHue', []);

app.controller('hueCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.hueUser = {};

    Homey.get('hueData', function (err, data) {
        $scope.hueData = angular.fromJson(data);
        $scope.$apply();
    });

    Homey.get('settings', function (err, settings) {
        if (settings) {
            $scope.settings = settings;
            $scope.$apply();
        };
    });

    $scope.saveConfig = function () {
        Homey.set('settings', $scope.settings);
    };

    $scope.setHueBrightness = function () {
        // app.get('/lights_bri/150', function (err, result) {
        //     if (err) return console.error(err);
        //     console.log(result);
        // });
        $http.get('https://570b6b1d9aac671e67accc63.homey.athom.com/api/app/com.internet/lights_bri/' + $scope.hueBrightness)
        
    };

}]);


// var hueUser = 'uFP-d5WlbURlh8lkeklSNowIuldbj0yBLty5xfuc';

// function doubleGroups(groups) {
//     return true;
// }

// app.controller('hueCtrl', function ($scope, $http, Api) {

//     console.log('app controller initialized');

//     $scope.formData = {};

//     $scope.setHueUser = function () {
//         Homey.set('hueUser', $scope.hueUser)
//     }

//     $scope.getHueUser = function () {
//         Homey.get('hueUser', function (err, hueUser) {
//             if (err) return console.error('Could not get hue user', err);
//             // got hueUser
//             $scope.$apply(function () {
//                 $scope.hueUser = hueUser
//             })

//         });
//     }

//     $scope.setHueBrightness = function () {
//         // $http.get('https://570b6b1d9aac671e67accc63.homey.athom.com/api/app/com.internet/lights_bri/' + $scope.hueBrightness)
//         // app.get('/lights_bri/150', function (err, result) {
//         //     if (err) return console.error(err);
//         //     console.log(result);
//         // });

//         Homey.ready()

//         // var api = Homey.manager('api');
//         // var flowCards = new api.App('com.internet');

//         // console.log('stap 1')
//         // flowCards.get('/bri_inc/255', function (err, result) {
//         //     console.log('stap 2')
//         //     if (err) return console.error(err);
//         //     console.log('stap 3')
//         //     console.log(result);
//         //     console.log('stap 4')
//         // });


//     }

//     $scope.lightsOff = function () {
//         console.log('lights off')
//         $http.get('https://570b6b1d9aac671e67accc63.homey.athom.com/api/app/com.internet/lights_bri/0')
//     }

//     $scope.lightsOn = function () {
//         $http.get('https://570b6b1d9aac671e67accc63.homey.athom.com/api/app/com.internet/lights_bri/255')
//     };


// });


// app.factory('Api', function ApiFactory($http) {
//     return {
//         getHueData: function () {
//             return $http({
//                 method: 'GET',
//                 url: 'http://192.168.2.1/api/' + hueUser
//             })
//         },
//         changeHueData: function (id, state) {
//             return $http({
//                 method: 'PUT',
//                 url: 'http://192.168.2.1/api/' + hueUser + '/groups/' + id + '/action/',
//                 data: { "bri_inc": state }
//             })
//         },
//         deleteHueData: function (id, state) {
//             return $http({
//                 method: 'DELETE',
//                 url: 'http://192.168.2.1/api/' + hueUser + '/groups/' + id
//             })
//         },
//         addHueData: function (newGroup) {
//             console.log('api stuff');
//             console.log(newGroup);
//             return $http({
//                 method: 'POST',
//                 url: 'http://192.168.2.1/api/' + hueUser + '/groups/',
//                 data: newGroup
//             })
//         }
//     };

// });

// app.filter();